const http = require('http')
const app = http.createServer((req, res)=>{
    if(req.url === '/'){
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write("<h3>Home Page</h3>");
        res.end();
    }else if(req.url === '/about'){
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write("<h3>About Page</h3>");
        res.end();
    }else if(req.url === '/admin'){
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write("<h3>Admin Page</h3>");
        res.end();
    }else{
        res.writeHead(200,{"Content-Type": "text/html"});
        res.write('<h1><center>404</center></h1>');
        res.end()
    }
    
})

const PORT = 3000

app.listen(PORT, ()=>{
console.log(`Server running at http://localhost:${PORT}`);
})
// console.log(http.METHODS)
// console.log(http.STATUS_CODES)